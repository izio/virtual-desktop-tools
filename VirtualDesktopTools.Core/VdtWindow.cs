﻿using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace VirtualDesktopTools.Core
{
    /// <summary>
    /// Base class for pages that inherits <see cref="MetroWindow"/> and implements <see cref="INotifyPropertyChanged"/>
    /// </summary>
    public class VdtWindow : MetroWindow, INotifyPropertyChanged
    { 
        /// <summary>
        /// sets the value <see cref="T"/> of the specified property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage">the storage property</param>
        /// <param name="value">the value to set</param>
        /// <param name="propertyName">the name of the property being set</param>
        public void Set<T>(ref T storage, T value, [CallerMemberName] string? propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;

            if (propertyName != null) OnPropertyChanged(propertyName);
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        #endregion
    }
}
