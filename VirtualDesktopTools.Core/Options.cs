﻿using CommandLine;

namespace VirtualDesktopTolls.Core
{
    /// <summary>
    /// Command line options
    /// </summary>
    public class Options
    {
        /// <summary>
        /// Specifies the number of virtual desktops to create
        /// </summary>
        [Option('c', "create", Required = false, HelpText = "The number of virtual desktops to create.")]
        public int Create { get; set; }

        /// <summary>
        /// The full path to the application to be launched
        /// </summary>
        [Option('l', "launch", Default = "", Required = false, HelpText = "The path to the application to launch.")]
        public string Launch { get; set; } = default!;

        /// <summary>
        /// The application to be moved
        /// </summary>
        [Option('m', "move", Default = "", Required = false, HelpText = "The application to be moved.")]
        public string Move { get; set; } = default!;

        /// <summary>
        /// The command line arguments to pass to the application to be run
        /// </summary>
        [Option('a', "arguments", Default = "", Required = false, HelpText = "The arguments to pass to the application to be run.")]
        public string Arguments { get; set; } = default!;

        /// <summary>
        /// The number of the virtual desktop to launch the application on
        /// </summary>
        [Option('o', "on", Required = false, Default = 1, HelpText = "The number of the virtual desktop to launch the application on.")]
        public int On { get; set; }

        /// <summary>
        /// The number of the virtual desktop to move the application to
        /// </summary>
        [Option('t', "to", Required = false, Default = 1, HelpText = "The number of the virtual desktop to move the application to.")]
        public int To { get; set; }

        /// <summary>
        /// Specifies that we should not switch to the new desktop
        /// </summary>
        [Option('n', "noswitch", Required = false, HelpText = "Prevent switching to the new desktop when the application is launched.")]
        public bool NoSwitch { get; set; }

        /// <summary>
        /// Specifies whether to automatically remove the desktop when the launched application closes
        /// </summary>
        [Option('r', "remove", Required = false, HelpText = "Remove the desktop that was created for the launched application.")]
        public bool Remove { get; set; }

        /// <summary>
        /// Specifies whether to create any virtual desktops
        /// </summary>
        public bool DoCreate => Create > 0;

        /// <summary>
        /// Specifies whether to launch an application
        /// </summary>
        public bool DoLaunch => !string.IsNullOrEmpty(Launch);

        /// <summary>
        /// Specifies the application to be moved
        /// </summary>
        public bool DoMove => !string.IsNullOrEmpty(Move);
    }
}