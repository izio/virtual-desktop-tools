﻿using System;

namespace VirtualDesktopTools.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessSummary
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IntPtr Handle { get; set; }
    }
}
