﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using VirtualDesktopTools.Core;
using WindowsDesktop;

namespace VirtualDesktopTools
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : VdtWindow
    {
        #region private properties

        private int _createDesktops;
        private string _launchApplication;
        private string _launchArguments;
        private int _launchOn;
        private bool _launchSwitch;
        private bool _launchRemove;
        private ObservableCollection<ProcessSummary> _moveProcesses;
        private ProcessSummary _moveProcess;
        private int _moveTo;
        private bool _moveSwitch;

        #endregion

        #region public properties

        public int CreateDesktops
        {
            get => _createDesktops;
            set => Set(ref _createDesktops, value);
        }

        public string LaunchApplication
        {
            get => _launchApplication;
            set => Set(ref _launchApplication, value);
        }

        public string LaunchArguments
        {
            get => _launchArguments;
            set => Set(ref _launchArguments, value);
        }

        public int LaunchOn
        {
            get => _launchOn;
            set => Set(ref _launchOn, value);
        }

        public bool LaunchSwitch
        {
            get => _launchSwitch;
            set => Set(ref _launchSwitch, value);
        }

        public bool LaunchRemove
        {
            get => _launchRemove;
            set => Set(ref _launchRemove, value);
        }

        public ObservableCollection<ProcessSummary> MoveProcesses
        {
            get => _moveProcesses;
            set => Set(ref _moveProcesses, value);
        }

        public ProcessSummary MoveProcess
        {
            get => _moveProcess;
            set => Set(ref _moveProcess, value);
        }

        public int MoveTo
        {
            get => _moveTo;
            set => Set(ref _moveTo, value);
        }

        public bool MoveSwitch
        {
            get => _moveSwitch;
            set => Set(ref _moveSwitch, value);
        }

        #endregion

        #region constructors

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;
        }

        #endregion

        #region event handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopCount = VirtualDesktop.GetDesktops().Length;

            LaunchSwitch = true;

            LaunchOn = 1;

            MoveTo = desktopCount;

            MoveSwitch = true;

            MoveProcesses = new ObservableCollection<ProcessSummary>();

            var processes = Process.GetProcesses().Where(p => (long)p.MainWindowHandle != 0);

            foreach (var process in processes)
            {
                MoveProcesses.Add(new ProcessSummary { Id = process.Id, Name = process.ProcessName, Handle = process.MainWindowHandle });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Create_Click(object sender, RoutedEventArgs e)
        {
            if (CreateDesktops > 10)
            {
                var result = MessageBox.Show($"Creating a large number of virtual desktops can cause your system to slow down. Are you sure you want to create {LaunchOn} virtual desktops?", "Create Desktops", MessageBoxButton.YesNo, default, MessageBoxResult.Yes);

                switch (result)
                {
                    case MessageBoxResult.No:

                        CreateDesktops = 0;

                        return;

                    default:

                        throw new ArgumentOutOfRangeException();
                }
            }

            for (var i = 0; i < CreateDesktops; i++)
            {
                VirtualDesktop.Create();
            }

            MoveTo += CreateDesktops;

            MessageBox.Show($"You have created {CreateDesktops} new virtual desktops.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PickApplication_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                DefaultExt = ".exe",
                Filter = "Applications (.exe)|*.exe"
            };

            var result = dialog.ShowDialog();

            if (result == true)
            {
                LaunchApplication = dialog.FileName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void LaunchApplication_Click(object sender, RoutedEventArgs e)
        {
            if (LaunchOn > 10)
            {
                var result = MessageBox.Show($"Creating a large number of virtual desktops can cause your system to slow down. Are you sure you want to create {LaunchOn} virtual desktops?", "Launch Application", MessageBoxButton.YesNo, default, MessageBoxResult.Yes);

                switch (result)
                {
                    case MessageBoxResult.No:

                        LaunchOn = 1;

                        return;

                    default:

                        throw new ArgumentOutOfRangeException();
                }
            }

            while (LaunchOn > VirtualDesktop.GetDesktops().Length)
            {
                VirtualDesktop.Create();
            }

            var targetDesktop = VirtualDesktop.GetDesktops()[Math.Max(0, LaunchOn - 1)];

            if (LaunchSwitch)
            {
                targetDesktop.Switch();
            }

            var startInfo = new ProcessStartInfo(LaunchApplication, LaunchArguments);

            try
            {
                if (Directory.Exists(Path.GetDirectoryName(LaunchApplication)))
                {
                    startInfo.WorkingDirectory = Path.GetDirectoryName(LaunchApplication);
                }
            }
            catch
            {
                //ignore error
            }

            var proc = Process.Start(startInfo);

            if (proc != null && !LaunchSwitch)
            {
                for (var backOff = 1; proc.MainWindowHandle.ToInt64() == 0 && backOff <= 0x1000; backOff <<= 1)
                {
                    Thread.Sleep(backOff);
                }

                if (proc.MainWindowHandle.ToInt64() != 0)
                {
                    VirtualDesktop.MoveToDesktop(proc.MainWindowHandle, targetDesktop);
                }
            }

            if (proc != null && LaunchRemove)
            {
                proc.WaitForExit();

                targetDesktop.Remove();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshProcesses_Click(object sender, RoutedEventArgs e)
        {
            MoveProcesses.Clear();

            var processes = Process.GetProcesses().Where(p => (long)p.MainWindowHandle != 0);

            foreach (var process in processes)
            {
                MoveProcesses.Add(new ProcessSummary { Id = process.Id, Name = process.ProcessName, Handle = process.MainWindowHandle });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void MoveApplication_Click(object sender, RoutedEventArgs e)
        {
            if (MoveTo > 10)
            {
                var result = MessageBox.Show($"Creating a large number of virtual desktops can cause your system to slow down. Are you sure you want to create {MoveTo} virtual desktops?", "Move Application", MessageBoxButton.YesNo, default, MessageBoxResult.Yes);

                switch (result)
                {
                    case MessageBoxResult.No:

                        MoveTo = VirtualDesktop.GetDesktops().Length;

                        return;

                    default:

                        throw new ArgumentOutOfRangeException();
                }
            }

            if (MoveProcess == null)
            {
                return;
            }

            while (MoveTo > VirtualDesktop.GetDesktops().Length)
            {
                VirtualDesktop.Create();
            }

            var targetDesktop = VirtualDesktop.GetDesktops()[Math.Max(0, MoveTo - 1)];

            if (MoveSwitch)
            {
                targetDesktop.Switch();
            }

            try
            {
                VirtualDesktop.MoveToDesktop(MoveProcess.Handle, targetDesktop);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"An error occurred moving the application. Exception: {exception.Message}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");

            e.Handled = regex.IsMatch(e.Text);
        }

        #endregion
    }
}
