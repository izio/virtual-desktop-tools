﻿using CommandLine;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using VirtualDesktopTolls.Core;
using WindowsDesktop;

namespace VirtualDesktopTools
{
    public partial class App : Application
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                var options = GetOptions();

                if (!VirtualDesktop.IsSupported)
                {
                    throw new NotSupportedException("Virtual Desktops are not supported on this system.");
                }

                if (options.DoCreate || options.DoLaunch || options.DoMove)
                {
                    if (options.DoCreate)
                    {
                        for (var i = 0; i < options.Create; i++)
                        {
                            VirtualDesktop.Create();
                        }
                    }

                    if (options.DoLaunch)
                    {
                        while (options.On > VirtualDesktop.GetDesktops().Length)
                        {
                            VirtualDesktop.Create();
                        }

                        var targetDesktop = VirtualDesktop.GetDesktops()[Math.Max(0, --options.On)];

                        if (!options.NoSwitch)
                        {
                            targetDesktop.Switch();
                        }

                        var startInfo = new ProcessStartInfo(options.Launch, options.Arguments);

                        try
                        {
                            if (Directory.Exists(Path.GetDirectoryName(options.Launch)))
                            {
                                startInfo.WorkingDirectory = Path.GetDirectoryName(options.Launch);
                            }
                        }
                        catch
                        {
                            //ignore error
                        }

                        var proc = Process.Start(startInfo);

                        if (proc != null && options.NoSwitch)
                        {
                            for (var backOff = 1; proc.MainWindowHandle.ToInt64() == 0 && backOff <= 0x1000; backOff <<= 1)
                            {
                                Thread.Sleep(backOff);
                            }

                            if (proc.MainWindowHandle.ToInt64() != 0)
                            {
                                VirtualDesktop.MoveToDesktop(proc.MainWindowHandle, targetDesktop);
                            }
                        }

                        if (proc != null && options.Remove)
                        {
                            proc.WaitForExit();

                            targetDesktop.Remove();
                        }
                    }

                    if (options.DoMove)
                    {
                        var processes = Process.GetProcessesByName(options.Move);

                        if (processes.Length == 0)
                        {
                            return;
                        }

                        while (options.To > VirtualDesktop.GetDesktops().Length)
                        {
                            VirtualDesktop.Create();
                        }

                        var targetDesktop = VirtualDesktop.GetDesktops()[Math.Max(0, --options.To)];

                        if (!options.NoSwitch)
                        {
                            targetDesktop.Switch();
                        }

                        foreach (var proc in processes)
                        {
                            if (proc.MainWindowHandle.ToInt64() != 0)
                            {
                                VirtualDesktop.MoveToDesktop(proc.MainWindowHandle, targetDesktop);
                            }
                        }
                    }

                    Current.Shutdown();
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show($"{ex.Message}:{Environment.NewLine}'{ex.FileName}'", "VDesk", MessageBoxButton.OK, MessageBoxImage.Hand);

                Current.Shutdown();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Virtual Desktop Tools");

                Current.Shutdown();
            }
        }

        /// <summary>
        /// Parses the command line arguments and returns a populated <see cref="Options"/> object
        /// </summary>
        /// <returns><see cref="Options"/></returns>
        /// <exception cref="ArgumentException"></exception>
        private Options GetOptions()
        {
            var result = Parser.Default.ParseArguments<Options>(Environment.GetCommandLineArgs());

            if (result.Errors.Any())
            {
                throw new ArgumentException("Unable to parse command line arguments.");
            }

            return result.Value;
        }
    }
}
