﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace VirtualDesktopTools.Converters
{
    /// <summary>
    /// An <see cref="IValueConverter"/> that will convert a non-null process to true
    /// </summary>
    public class NotNullEnabledConverter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return true;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
