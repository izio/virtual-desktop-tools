﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace VirtualDesktopTools.Converters
{
    /// <summary>
    /// An <see cref="IValueConverter"/> that will convert a non-empty string to true
    /// </summary>
    public class StringEnabledConverter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string)value))
            {
                return false;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
