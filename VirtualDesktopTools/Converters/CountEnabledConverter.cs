﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace VirtualDesktopTools.Converters
{
    /// <summary>
    /// An <see cref="IValueConverter"/> that will convert a value greater than zero to true
    /// </summary>
    public class CountEnabledConverter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && (int)value > 0)
            {
                return true;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || (bool)value)
            {
                return true;
            }

            return false; ;
        }

        #endregion
    }
}
